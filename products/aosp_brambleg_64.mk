#
# GMS package includes a few prebuilt AOSP modules which should be preloaded
# in the system partition. To make sure the GMS build configuration works
# with the Android Build system, PRODUCT_ENFORCE_ARTIFACT_PATH_REQUIREMENTS
# is set to 'relaxed' and an allowlist of GMS modules in the system partition
# is added here. For more information, please refer to
# https://source.android.com/devices/bootloader/partitions/product-interfaces
PRODUCT_ENFORCE_ARTIFACT_PATH_REQUIREMENTS := relaxed
include vendor/partner_gms/products/gms_product_enforcement_allow_list.mk

$(call inherit-product, device/google/bramble/aosp_bramble_64.mk)

# Add ADB vendor keys for build testing
$(call inherit-product, vendor/google/security/adb/vendor_key.mk)

# Uncomment following two lines if you want to build with mainline modules
#MAINLINE_INCLUDE_WIFI_MODULE := false
#$(call inherit-product, vendor/partner_modules/build/mainline_modules.mk)

$(call inherit-product, vendor/partner_gms/products/gms_64bit_only.mk)
#$(call inherit-product, vendor/partner_gms/products/gms_eea_v2_type4c.mk)

PRODUCT_PACKAGES += \
    drmserver \
    libdrmframework \
    libdrmframework_jni

PRODUCT_PROPERTY_OVERRIDES += \
    drm.service.enabled=true \
    media.mediadrmservice.enable=true

PRODUCT_MANUFACTURER := Google
PRODUCT_BRAND := google
PRODUCT_NAME := aosp_brambleg_64
PRODUCT_DEVICE := bramble
PRODUCT_MODEL := aosp_brambleg_64
PRODUCT_SHIPPING_API_LEVEL := 30

# com.android.internal.systemui.navbar.threebutton: use classic 3-button mode by default
# com.android.internal.systemui.navbar.gestural: use gesture navigation mode by default
PRODUCT_PRODUCT_PROPERTIES += ro.boot.vendor.overlay.theme=com.android.internal.systemui.navbar.gestural

PRODUCT_PACKAGES += AndroidGmsBetaOverlay

PRODUCT_BROKEN_VERIFY_USES_LIBRARIES := false

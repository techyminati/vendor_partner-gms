/*
 * Copyright (C) 2022 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.android.emergency;

import java.util.regex.Pattern;

/**
 * Contract class for RestoreProvider.
 */
public interface RestoreProviderContract {

    /** Code for UriMatcher. */
    public static final int MEDICAL_INFO = 0;
    public static final int EMERGENCY_CONTACT = 1;

    /** {@link android.content.ContentProvider} authority. */
    public static final String AUTHORITY = "com.android.emergency.restore";

    /** Method name for {@link android.content.ContentProvider#call} */
    public static final String CHECK_MIGRATION_STATE = "checkMigrationState";
    public static final String SET_MIGRATION_COMPLETED = "setMigrationCompleted";

    public static final String CONTACT_SEPARATOR = "|";
    public static final String QUOTE_CONTACT_SEPARATOR = Pattern.quote(CONTACT_SEPARATOR);

    /** Path for accessing medical info. */
    public static final String MEDICAL_INFO_URI_PATH = "medicalinfo";

    /** Path for accessing emergency contact. */
    public static final String EMERGENCY_CONTACT_URI_PATH = "emergencycontact";

    /** Key for emergency contacts preference */
    public static final String KEY_EMERGENCY_CONTACTS = "emergency_contacts";

    /** Key to read the migration state */
    public static final String KEY_MIGRATION_STATE = "migrated_state";

    /** Key to read the address of the user. */
    public static final String KEY_ADDRESS = "address";

    /** Key to read the blood type of the user. */
    public static final String KEY_BLOOD_TYPE = "blood_type";

    /** Key to read the allergies of the user. */
    public static final String KEY_ALLERGIES = "allergies";

    /** Key to read the medications of the user. */
    public static final String KEY_MEDICATIONS = "medications";

    /** Key to read the medical conditions of the user. */
    public static final String KEY_MEDICAL_CONDITIONS = "medical_conditions";

    /** Key to read the organ donor choice of the user. */
    public static final String KEY_ORGAN_DONOR = "organ_donor";

    /** Keys for all emergency info preferences. */
    public static final String[] KEYS_EDIT_EMERGENCY_INFO = {
            KEY_ADDRESS,
            KEY_BLOOD_TYPE,
            KEY_ALLERGIES,
            KEY_MEDICATIONS,
            KEY_MEDICAL_CONDITIONS,
            KEY_ORGAN_DONOR
    };
}

